package com.example.credits.controller;

import com.example.credits.model.LoginForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class LoginController {

    @GetMapping("/")
    public String Logging(ModelMap modelMap){

        modelMap.addAttribute("loginForm", new LoginForm());
        return "login";
    }
    @PostMapping("/login")
    public String Logging(@ModelAttribute @Valid LoginForm loginForm, BindingResult result, ModelMap modelMap){



        if (result.hasErrors()){
            modelMap.addAttribute("info","Nieprawidłowe dane");
            return "login";
        }
        modelMap.addAttribute("info", loginForm.getLogin()+" zalogowany ");
        return "redirect:/main";
    }

}

