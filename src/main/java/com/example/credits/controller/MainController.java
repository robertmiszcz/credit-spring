package com.example.credits.controller;

import com.example.credits.model.RequestForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MainController {

    @GetMapping("/main")
    public String credit(ModelMap modelMap){

        modelMap.addAttribute("requestForm",new RequestForm());

        return "main";
    }

    @PostMapping("/main")
    public String credit(@ModelAttribute RequestForm requestForm, ModelMap modelMap){

           modelMap.addAttribute("infoCredit", canGetCredit(requestForm));

        return "main";
    }

    private boolean canGetCredit(RequestForm form){

        return (form.getMonthCash() - form.getMinusCash()) * 0.7 > form.getCreditCash()/form.getCreditTime();
    }
}
