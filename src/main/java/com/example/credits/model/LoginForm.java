package com.example.credits.model;

import javax.validation.constraints.*;


public class LoginForm {
    @Size(min=2 ,max=25)
    @NotBlank
    private String login;
    @Size(min=2 ,max=25)
    @NotBlank
    private String password;

    public LoginForm() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LoginForm(String login, String password) {

        this.login = login;
        this.password = password;
    }
}
